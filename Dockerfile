# Use an appropriate base image, such as NGINX
FROM nginx

# Copy all files from the current directory to the NGINX web root
COPY . /usr/share/nginx/html

# Set the default entrypoint to index.html
ENTRYPOINT ["nginx", "-g", "daemon off;"]