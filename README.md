# Monument of Life
(***Version 1***) A virtual reality world for Atemosta's current patrons @ [http://mol-v1.atos.to](http://mol-v1.atos.to)

## Deployment
### Local
Install Node 20.0.0 (using NVM) and run the following commands
```
npm i
npm run dev
```

### Docker
Install Docker WITH Docker Compose and run the following commands
```
chmod +x redeploy.sh // Creates an executable script
./redeploy.sh // Pulls the latest changes from the remote repo, takes down the current Docker deployment, rebuilds the Docker image, and redeploys it!
```

## Donate/Subscribe
![Monument of Life from the hit anime, Sword Art Online](/assets/images/lb.jpg)
Consider donating or subscribing (and optionally, be added to the VR world) at [https://liberapay.com/Osco](https://liberapay.com/Osco)
## Inspiration
![Monument of Life from the hit anime, Sword Art Online](/assets/images/mol.png)

This VR world is heavily inspired by the [Monunment of Life](https://swordartonline.fandom.com/wiki/Monument_of_Life) from Sword Art Online
